import type { Express, Request, Response } from "express";
import { Router } from "express";
import { ApiHandler } from "../handlers";

export function addIndexRoutes(app: Express) {
  const router = Router();

  router.get(
    "/",
    generateHandlerWithBody(() => {
      return { status: 200, body: { hello: "world" } };
    })
  );
  app.use("/", router);
}

export function generateHandlerWithBody<T>(
  handler: ApiHandler<T>,
  adapter: RequestAdapter<T> = UselessAdapter
) {
  return (request: Request, response: Response) => {
    const { status, body } = handler(adapter(request.body));
    response.type("json").status(status).send(body);
  };
}

export function generateHandlerWithParams(
  handler: ApiHandler<Record<string, string>>
) {
  return (request: Request, response: Response) => {
    const { status, body } = handler(request.params);
    response.type("json").status(status).send(body);
  };
}

/**
 * An adapter whose purpose is to validate and transform the data coming from
 * a request to the desired data format for the handler.
 */
interface RequestAdapter<T> {
  (body: unknown): T;
}

function UselessAdapter<T>(body: unknown) {
  // This adapter is NOT an adapter - we are just casting the type so that
  // Typescript won't complain. It is not recommended to use such an approach
  // in real code, and is used here as a fallback implementation.
  return body as T;
}
