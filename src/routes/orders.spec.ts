import express from "express";
import request from "supertest";
import { buildOrder } from "../factories/order";
import { clearAll } from "../handlers/orders";
import { mockApp } from "../testUtils";
import { addOrderRoutes } from "./orders";

describe("Orders", () => {
  afterEach(() => {
    clearAll();
  });

  it("registers the orders on the correct endpoint", () => {
    const app = mockApp();
    addOrderRoutes(app);
    expect(app.use).toHaveBeenCalledWith("/orders", expect.anything());
  });

  /*
   * These tests are integration tests to check that everything is wired
   * together correctly.
   */
  it("creates an order", async () => {
    const app = express();
    app.use(express.json());
    addOrderRoutes(app);
    const order = buildOrder();
    const response = await request(app)
      .post("/orders")
      .set("Accept", "application/json")
      .send(order);
    expect(response.status).toEqual(201);
    expect(response.body).toEqual({ id: 1, ...order });
  });

  it("lists existing orders", async () => {
    const app = express();
    app.use(express.json());
    addOrderRoutes(app);
    const order = buildOrder();

    // Create 3 orders
    await request(app)
      .post("/orders")
      .set("Accept", "application/json")
      .send(order);
    await request(app)
      .post("/orders")
      .set("Accept", "application/json")
      .send(order);
    await request(app)
      .post("/orders")
      .set("Accept", "application/json")
      .send(order);

    const response = await request(app)
      .get("/orders")
      .set("Accept", "application/json");
    expect(response.status).toEqual(200);
    expect(response.body).toEqual([
      { id: 1, ...order },
      { id: 2, ...order },
      { id: 3, ...order },
    ]);
  });

  it("shows a specific order", async () => {
    const app = express();
    app.use(express.json());
    addOrderRoutes(app);
    const order = buildOrder();

    await request(app)
      .post("/orders")
      .set("Accept", "application/json")
      .send(order);

    const response = await request(app)
      .get("/orders/1")
      .set("Accept", "application/json");
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({ id: 1, ...order });
  });

  it.todo("deletes an order");

  it.todo("modifies an order");
});
