import type { Express } from "express";
import { Router } from "express";
import { create, list } from "../handlers/orders";
import { generateHandlerWithBody, generateHandlerWithParams } from "../routes";

export function addOrderRoutes(app: Express) {
  const router = Router();

  router.post("/", generateHandlerWithBody(create));
  router.get("/", generateHandlerWithBody(list));
  router.get(
    "/:id",
    generateHandlerWithParams((params) => ({
      status: 200,
      body: { id: params.id },
    }))
  );
  app.use("/orders", router);
}
