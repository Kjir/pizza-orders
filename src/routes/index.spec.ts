import type { Request, Response } from "express";
import {
  addIndexRoutes,
  generateHandlerWithBody,
  generateHandlerWithParams,
} from ".";
import { mockApp } from "../testUtils";

describe("Index routes", () => {
  it("adds an example route", () => {
    const app = mockApp();
    addIndexRoutes(app);
    expect(app.use).toHaveBeenCalledWith("/", expect.anything());
  });

  it("responds with hello world", () => {
    /**
     * This test relies on the internal behaviour of a Router in the express
     * module. It's not a good way to test because it is hard to understand why
     * we access certain properties.
     *
     * I left this test to show why for the others I chose a different
     * approach that makes everything much easier to test.
     */
    const app = mockApp();
    addIndexRoutes(app);
    const router = app.use.mock.calls[0]?.[1];
    const request = { method: "get" };
    const response = mockExpressResponse();
    router?.stack[0].handle(request, response, jest.fn());
    expect(response.send).toHaveBeenCalledWith({ hello: "world" });
    expect(response.status).toHaveBeenCalledWith(200);
  });
});

describe("generate handler", () => {
  it("sets the content type, status code and content of the response", () => {
    const handler = () => ({ status: 404, body: { error: "Not found" } });
    const expressHandler = generateHandlerWithBody(handler);
    const response = mockExpressResponse();
    expressHandler({} as Request, response);
    expect(response.type).toHaveBeenCalledWith("json");
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.send).toHaveBeenCalledWith({ error: "Not found" });
  });

  it("passes the content of the body to the handler", () => {
    const handler = jest.fn().mockReturnValue({ status: 200, body: "" });
    const expressHandler = generateHandlerWithBody(handler);
    const body = { a: 1, b: "hello" };
    expressHandler({ body } as Request, mockExpressResponse());
    expect(handler).toHaveBeenCalledWith(body);
  });

  it("passes the params to the handler", () => {
    const handler = jest.fn().mockReturnValue({ status: 200, body: "" });
    const expressHandler = generateHandlerWithParams(handler);
    const params = { id: "3" };
    expressHandler({ params } as unknown as Request, mockExpressResponse());
    expect(handler).toHaveBeenCalledWith(params);
  });
});

function mockExpressResponse() {
  return {
    type: jest.fn().mockImplementation(chainableResponseMethod),
    status: jest.fn().mockImplementation(chainableResponseMethod),
    send: jest.fn().mockImplementation(chainableResponseMethod),
  } as unknown as Response;
}

function chainableResponseMethod(this: Response) {
  return this;
}
