export function buildOrder() {
  return {
    pizzas: [
      { name: "Prosciutto", price: 10 },
      { name: "Salami", price: 12 },
    ],
    total: 22,
    address: "Glutton Street 19",
    zip: "GLT-123",
    city: "Fooditon",
  };
}
