import { buildOrder } from "../factories/order";
import { clearAll, create, list, show } from "./orders";

describe("Orders", () => {
  afterEach(() => {
    clearAll();
  });
  it("creates a new order", () => {
    const order = buildOrder();
    const response = create(order);
    expect(response.status).toEqual(201);
    expect(response.body).toEqual({ id: 1, ...order });
  });

  it("creates multiple new orders when posting the same order", () => {
    const order = buildOrder();
    const response1 = create(order);
    const response2 = create(order);
    const response3 = create(order);
    expect(response1.body.id).toEqual(1);
    expect(response2.body.id).toEqual(2);
    expect(response3.body.id).toEqual(3);
  });

  it("returns an empty list of orders", () => {
    const { status, body } = list();
    expect(status).toEqual(200);
    expect(body).toEqual([]);
  });

  it("returns the list of existing orders", () => {
    const order = buildOrder();
    const { body: order1 } = create(order);
    const { body: order2 } = create(order);
    const { status, body } = list();
    expect(status).toEqual(200);
    expect(body).toEqual([order1, order2]);
  });

  it("returns a single order", () => {
    const order = buildOrder();
    const { body: createdOrder } = create(order);
    const { status, body: returnedOrder } = show({ id: createdOrder.id });
    expect(status).toEqual(200);
    expect(returnedOrder).toEqual(createdOrder);
  });

  it.todo("returns a not found response if the order is missing");
});
