interface Pizza {
  name: string;
  price: number;
}
interface Order {
  id: number;
  pizzas: Pizza[];
  total: number;
  address: string;
  zip: string;
  city: string;
}

/**
 * For simplicity we are using a global variable to store orders in-memory. In
 * a real application it would be in a database or some other form of storage.
 */
let orders: Record<Order["id"], Order> = {};

export function create(order: Omit<Order, "id">) {
  const savedOrder = { id: generateId(), ...order };
  orders[savedOrder.id] = savedOrder;
  return { status: 201, body: savedOrder };
}

export function list() {
  return { status: 200, body: Object.values(orders) };
}

function generateId() {
  const existingIds = Object.keys(orders).map((id) => parseInt(id));
  return Math.max(0, ...existingIds) + 1;
}

/**
 * This function is needed to clear the orders between tests.
 */
export function clearAll() {
  orders = {};
}
