interface JsonResponse {
  status: number;
  body: Record<string, any> | "";
}

export interface ApiHandler<T> {
  (parameters: T): JsonResponse;
}
