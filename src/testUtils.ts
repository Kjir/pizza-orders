import type { Express } from "express";

export function mockApp(): jest.Mocked<Express> {
  return {
    use: jest.fn(),
  } as unknown as jest.Mocked<Express>;
}
