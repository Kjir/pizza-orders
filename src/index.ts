import express from "express";
import { addIndexRoutes } from "./routes/index.js";
import { addOrderRoutes } from "./routes/orders.js";

function startServer() {
  const app = express();
  app.use(express.json());
  addIndexRoutes(app);
  addOrderRoutes(app);
  app.listen(3000);
}

startServer();
