# Pizza orders

This small application is an API that allows to save orders for Pizzas!

The application is developed with a TDD approach. Some tests are written but
the implementation is missing; some other tests are only written as TODOs,
leaving it as an exercise to begin with the test first and then add the
implementation too.

The commit history is relevant: each commit adds a step and - with the
exception of the very first commits - each step adds a test first and the
implementation afterwards. After the first TDD tests, the later steps use an
Acceptance Test-Driven Development approach.

## Installation

The project uses [pnpm](https://pnpm.io) to manage dependencies. To install them:

```bash
pnpm install
```

## Running the server

The server can be started by executing:

```bash
pnpm serve
```

## Running tests

Tests can be executed with the following command:

```bash
pnpm test
```

To run them automatically every time a file has changed:

```bash
pnpm test -- --watch
```

## Tasks

1. Review existing commits
2. Add the implementation of the method to show a single order
3. Add the acceptance test to remove an order (DELETE verb)
4. Add the test for the handler to remove an order
5. Add the implementation of the remove order handler
6. Route the remove order handler to the correct endpoint
7. Add the acceptance test for the edit operation (PUT verb)
8. Add the unit tests
9. Implement the edit functionality
